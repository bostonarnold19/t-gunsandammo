<!DOCTYPE html>
<html lang="en">
    <head>
        @include('partials.auth._head')
        @yield('styles')
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100" style="background-image: url('login-v3/images/bg-01.jpg');">
                @yield('content')
            </div>
        </div>
        <div id="dropDownSelect1"></div>
        @include('partials.auth._javascript')
        @yield('scripts')
    </body>
</html>
