<!DOCTYPE html>
<html lang="en">
    <head>
        @include('partials.main._head')
        @yield('styles')
    </head>
    <body>
        @include('partials.main._nav')
        @yield('content')
        @include('partials.main._footer')
        @include('partials.main._javascript')
        @yield('scripts')
    </body>
</html>
