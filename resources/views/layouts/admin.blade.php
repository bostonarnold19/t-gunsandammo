<!DOCTYPE html>
<html lang="en">
    <head>
        @include('partials.admin._head')
        @yield('styles')
    </head>
    <body>
        <div id="wrapper">
            @include('partials.admin._nav')
            <div id="page-wrapper">
                @yield('content')
            </div>
        </div>
        @include('partials.admin._javascript')
        @yield('scripts')
    </body>
</html>
