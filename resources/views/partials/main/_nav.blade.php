<header id="header">
  <nav class="navbar navbar-default navbar-static-top" role="banner">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand">
          <a href="{{route('home')}}"><h1>EGA</h1></a>
        </div>
      </div>
      <div class="navbar-collapse collapse">
        <div class="menu">
          <ul class="nav nav-tabs" role="tablist">
            @if(Auth::guest())
            <li role="presentation"><a href="{{route('login')}}">Login</a></li>
            <li role="presentation"><a href="{{route('register')}}">Register</a></li>
            @endif
            @if(Auth::user())
            @if(auth()->user()->hasRole('Client'))
            <li role="presentation"><a href="{{route('order-client.index')}}" class="{{Request::is('order-client.index') ? 'active' : ''}}">Order</a></li>
            @endif
            @if(auth()->user()->hasRole(['Admin', 'Cashier']))
            <li role="presentation"><a href="{{route('dashboard')}}" class="{{Request::is('dashboard') ? 'active' : ''}}">Panel</a></li>
            @endif
            @if(auth()->user()->hasRole(['Client']))
            <li role="presentation"><a href="{{route('cart.index')}}" class="{{Request::is('cart') ? 'active' : ''}}">Cart</a></li>
            @endif
            <li role="presentation"><a href="{{route('categories')}}" class="{{Request::is('categories') ? 'active' : ''}}">Products</a></li>
            <li role="presentation"><a onclick="document.getElementById('form-logout').submit();" href="#">Logout</a></li>
            @endif
          </ul>
          @if(Auth::user())
          <form id="form-logout" method="POST" action="{{ route('logout') }}">
            {{ csrf_field() }}
          </form>
          @endif
        </div>
      </div>
    </div>
  </nav>
</header>
