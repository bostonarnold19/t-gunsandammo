<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>EGA</title>
<link href="{{ asset('day/css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('day/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('day/css/animate.css') }}">
<link href="{{ asset('day/css/animate.min.css') }}" rel="stylesheet">
<link href="{{ asset('day/css/style.css') }}" rel="stylesheet" />
<style>
    .mgt-t0 {
        margin-top: 0;
    }
    figure.effect-marley figcaption {
         text-align: left;
    }
    .gallery figure figcaption {
        text-transform: none;
        font-size: 1em;
    }
    .text-white {
        color: white;
    }
    .text-red {
        color: red;
    }
</style>
