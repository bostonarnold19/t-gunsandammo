<script src="{{ asset('day/js/jquery.js') }}"></script>
<script src="{{ asset('day/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('day/js/wow.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert2@7.1.2/dist/sweetalert2.all.js"></script>
<script>
    wow = new WOW({}).init();
</script>
