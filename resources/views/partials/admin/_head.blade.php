<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>EGA | Panel</title>
<link rel="stylesheet" type="text/css" href="{{ asset('deep-blue-admin/bootstrap/css/bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('deep-blue-admin/font-awesome/css/font-awesome.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('deep-blue-admin/css/local.css') }}" />
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
<style>
    .mgt-b10 {
        margin-bottom: 10px;
    }
    .text-red {
        color: red;
    }
</style>
