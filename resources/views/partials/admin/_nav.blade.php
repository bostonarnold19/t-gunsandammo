<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ auth()->user()->hasRole('Client') ? route('cart.index') : route('dashboard') }}">EGA Panel</a>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            @if(Auth::user())
            @if(auth()->user()->hasRole(['Admin', 'Cashier']))
            <li class="{{Request::is('dashboard') ? 'active' : ''}}">
                <a href="{{route('dashboard')}}"><i class="fa fa-bullseye"></i> Dashboard</a>
            </li>
            @endif
            @if(auth()->user()->hasRole('Admin'))
            <li class="{{Request::is('category', 'category/*') ? 'active' : ''}}">
                <a href="{{route('category.index')}}"><i class="fa fa-tags"></i> Category</a>
            </li>
            <li class="{{Request::is('supplier', 'supplier/*') ? 'active' : ''}}">
                <a href="{{route('supplier.index')}}"><i class="fa fa-users"></i> Supplier</a>
            </li>
            <li class="{{Request::is('product', 'product/*') ? 'active' : ''}}">
                <a href="{{route('product.index')}}"><i class="fa fa-shopping-bag"></i> Product</a>
            </li>
            <li class="{{Request::is('stock', 'stock/*') ? 'active' : ''}}">
                <a href="{{route('stock.index')}}"><i class="fa fa-list-alt"></i> Stock</a>
            </li>
            @endif
            @if(auth()->user()->hasRole(['Admin', 'Cashier']))
            <li class="{{Request::is('order', 'order/*') ? 'active' : ''}}">
                <a href="{{route('order.index')}}"><i class="fa fa-shopping-cart"></i> Order</a>
            </li>
            <li class="{{Request::is('transaction', 'transaction/*') ? 'active' : ''}}">
                <a href="{{route('transaction.index')}}"><i class="fa fa-exchange"></i> Transaction</a>
            </li>
            @endif
            @if(auth()->user()->hasRole('Client'))
            <li class="{{Request::is('cart', '/*') ? 'active' : ''}}">
                <a href="{{route('cart.index')}}"><i class="fa fa-shopping-cart"></i> Cart</a>
            </li>
            <li class="{{Request::is('order-client', 'order-client/*') ? 'active' : ''}}">
                <a href="{{route('order-client.index')}}"><i class="fa fa-shopping-cart"></i> Orders</a>
            </li>
            @endif
            @if(auth()->user()->hasRole('Admin'))
            <li class="{{Request::is('payroll', 'payroll/*') ? 'active' : ''}}">
                <a href="{{route('payroll.index')}}"><i class="fa fa-paypal"></i> Payroll</a>
            </li>
            <li class="{{Request::is('user', 'user/*') ? 'active' : ''}}">
                <a href="{{route('user.index')}}"><i class="fa fa-user"></i> User Management</a>
            </li>
            @endif
            @if(auth()->user()->hasRole('Cashier'))
            <li class="{{Request::is('payslip', 'payslip/*') ? 'active' : ''}}">
                <a href="{{route('payslip.index')}}"><i class="fa fa-user"></i> Payslip</a>
            </li>
            @endif
            @endif
        </ul>
        <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{auth()->user()->first_name}}<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a onclick="document.getElementById('form-logout').submit()" href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
                </ul>
            </li>
        </ul>
        @if(Auth::user())
        <form id="form-logout" method="POST" action="{{ route('logout') }}">
            {{ csrf_field() }}
        </form>
        @endif
    </div>
</nav>
