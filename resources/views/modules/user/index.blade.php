@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>User <small>User list</small></h1>
        <a href="{{route('user.create')}}" class="btn btn-info mgt-b10">Create User</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> User list</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->first_name}} {{$user->middle_name}} {{$user->last_name}}</td>
                                <td>{{$user->username}}</td>
                                <td>
                                        @foreach($user->roles as $role)
                                        {{$role->name}},
                                        @endforeach
                                    </td>
                                <td>
                                    @if(!$user->hasRole('Client'))
                                    <a href="{{route('user.edit', $user->id)}}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                                    @else
                                    <a href="{{route('transaction.index', ['customer_id' => $user->id])}}" class="btn btn-success"><i class="fa fa-exchange"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection
