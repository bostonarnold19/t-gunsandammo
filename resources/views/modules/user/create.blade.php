@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>User <small>User create</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> User create</h3>
            </div>
            <div class="panel-body">
                <form action="{{route('user.store')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input required name="first_name" value="{{old('first_name')}}" class="form-control" placeholder="First Name">
                            </div>
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input name="middle_name" value="{{old('middle_name')}}" class="form-control" placeholder="Middle Name">
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input name="last_name" value="{{old('last_name')}}" class="form-control" placeholder="Last Name">
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <select class="form-control" name="role_id" id="role_id" required>
                                    <option {{ old('role_id') == null ? 'selected' : '' }} disabled>Select Role</option>
                                    @foreach($roles->except(3) as $role)
                                    <option {{ old('role_id') == $role->id ? 'selected' : '' }} value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Rate (Daily)</label>
                                <input {{ old('role_id') == 2 ? '' : 'disabled' }} id="day_rate" type="number" min="0" name="day_rate" value="{{old('day_rate')}}" class="form-control" placeholder="Daily Rate">
                            </div>
                            <div class="form-group">
                                <label>Rate (Hourly)</label>
                                <input {{ old('role_id') == 2 ? 'readonly' : 'disabled' }} type="number" min="0" id="rate" name="rate" value="{{old('rate')}}" class="form-control" placeholder="Hourly Rate">
                            </div>
                            <div class="form-group">
                                <label>RFID</label>
                                <input {{ old('role_id') == 2 ? '' : 'disabled' }} id="rfid" name="rfid" value="{{old('rfid')}}" class="form-control" placeholder="RFID">
                            </div>
                            <div class="form-group">
                                <label>Hired Date</label>
                                <input {{ old('role_id') == 2 ? '' : 'disabled' }} id="hired_date" name="hired_date" type="date" value="{{old('hired_date')}}" class="form-control" placeholder="Hired Date">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success form-control">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#day_rate').on('keyup', function(){
            var day_rate = $(this).val();
            var rate = (day_rate / 8);
            $('#rate').val(rate.toFixed(2));
        });
        $('#role_id').on('change', function() {
            if($(this).val() == 2) {
                $('#rate').prop( "disabled", false);
                $('#rate').prop( "readonly", true);
                $('#day_rate').prop( "disabled", false);
                $('#hired_date').prop( "disabled", false);
                $('#rfid').prop( "disabled", false);
            } else {
                $('#rate').prop( "disabled", true);
                $('#day_rate').prop( "disabled", true);
                $('#hired_date').prop( "disabled", true);
                $('#rfid').prop( "disabled", true);
            }
        });
    </script>
@stop
