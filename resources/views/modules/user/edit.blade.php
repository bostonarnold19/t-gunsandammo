@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>User <small>User Edit</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> User Edit</h3>
            </div>
            <div class="panel-body">
                <form action="{{route('user.update', $user->id)}}" method="POST">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input required name="first_name" value="{{$user->first_name}}" class="form-control" placeholder="First Name">
                            </div>
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input name="middle_name" value="{{$user->middle_name}}" class="form-control" placeholder="Middle Name">
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input name="last_name" value="{{$user->last_name}}" class="form-control" placeholder="Last Name">
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <select class="form-control" name="role_id" id="role_id" required>
                                    <option {{ $user->roles->first()->id == null ? 'selected' : '' }} disabled>Select Role</option>
                                    @foreach($roles->except(3) as $role)
                                    <option {{ $user->roles->first()->id == $role->id ? 'selected' : '' }} value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Rate (Daily)</label>
                                <input {{ $user->hasRole('Cashier') ? '' : 'disabled' }} type="number" min="0" id="day_rate" name="day_rate" class="form-control" placeholder="Daily Rate">
                            </div>
                            <div class="form-group">
                                <label>Rate (Hourly)</label>
                                <input {{ $user->hasRole('Cashier') ? 'readonly' : 'disabled' }} type="number" min="0" id="rate" name="rate" value="{{$user->employeeInfo->rate}}" class="form-control" placeholder="Hourly Rate">
                            </div>
                            <div class="form-group">
                                <label>RFID</label>
                                <input {{ $user->hasRole('Cashier') ? '' : 'disabled' }} id="rfid" name="rfid" value="{{$user->employeeInfo->rfid}}" class="form-control" placeholder="RFID">
                            </div>
                            <div class="form-group">
                                <label>Hired Date</label>
                                <input {{ $user->hasRole('Cashier') ? '' : 'disabled' }} id="hired_date" name="hired_date" type="date" value="{{$user->employeeInfo->hired_date}}" class="form-control" placeholder="Hired Date">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success form-control">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var hour_rate = $('#rate').val();
        $('#day_rate').val((hour_rate * 8))
        $('#day_rate').on('keyup', function(){
            var day_rate = $(this).val();
            var rate = (day_rate / 8);
            $('#rate').val(rate.toFixed(2));
        });
        $('#role_id').on('change', function() {
            if($(this).val() == 2) {
                $('#rate').prop( "disabled", false);
                $('#rate').prop( "readonly", true);
                $('#day_rate').prop( "disabled", false);
                $('#hired_date').prop( "disabled", false);
                $('#rfid').prop( "disabled", false);
            } else {
                $('#rate').prop( "disabled", true);
                $('#day_rate').prop( "disabled", true);
                $('#hired_date').prop( "disabled", true);
                $('#rfid').prop( "disabled", true);
            }
        });
    </script>
@stop
