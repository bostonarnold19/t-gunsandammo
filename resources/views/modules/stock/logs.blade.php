@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Stock <small>Stock logs</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Stock logs</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>Supplier</th>
                                <th>Employee</th>
                                <th>Serial Number</th>
                                <th>Initial Quantity</th>
                                <th>Unit Price</th>
                                <th>Total Price</th>
                                <th>Remarks</th>
                                <th>Purshased Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($stocks as $stock)
                            <tr>
                                <td>{{$stock->id}}</td>
                                <td>{{$stock->product->name}}</td>
                                <td>{{$stock->supplier->name}}</td>
                                <td>{{$stock->user->first_name}} {{$stock->user->last_name}}</td>
                                <td>{{$stock->serial_number}}</td>
                                <td>{{$stock->initial_quantity}}</td>
                                <td>{{$stock->unit_price}}</td>
                                <td>{{$stock->initial_quantity * $stock->unit_price}}</td>
                                <td>{{$stock->remarks}}</td>
                                <td>{{$stock->purchased_date}}</td>
                                <td>
                                    <a href="{{route('stock.edit', $stock->id)}}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                                    <button type="submit" form="stock-delete-{{$stock->id}}" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                    <form onsubmit="return confirm('Do you want to delete this data?');" id="stock-delete-{{$stock->id}}" action="{{route('stock.destroy', $stock->id)}}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection
