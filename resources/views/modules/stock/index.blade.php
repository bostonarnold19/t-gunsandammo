@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Stock <small>Stock list</small></h1>
        <a href="{{route('stock.create')}}" class="btn btn-info mgt-b10">Create Stock</a>
        <a href="{{route('stock.logs')}}" class="btn btn-info mgt-b10">Stock Logs</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Stock list</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>Type</th>
                                <th>Caliber</th>
                                <th>Remaining Stocks</th>
                                <th>Total Stocks</th>
                                <th>Total Sold</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($stocks as $stock)
                            <tr>
                                <td>{{$stock['id']}}</td>
                                <td>{{$stock['product']}}</td>
                                <td>{{$stock['type']}}</td>
                                <td>{{$stock['caliber']}}</td>
                                <td>{{$stock['remaining_stocks']}}</td>
                                <td>{{$stock['total_stocks']}}</td>
                                <td>{{$stock['total_sold']}}</td>
                                <td>{{$stock['price']}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection
