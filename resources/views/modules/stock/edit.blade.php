@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Stock <small>Stock Edit</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Stock Edit</h3>
            </div>
            <div class="panel-body">
                <form action="{{route('stock.update', $stock->id)}}" method="POST">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Product</label>
                                <select required name="product_id" class="form-control">
                                    <option {{ empty($stock->product) === null ? 'selected' : '' }} disabled>Select Product</option>
                                    @foreach($products as $product)
                                    <option value="{{$product->id}}" @if(!empty($stock->product)) {{ $stock->product->id === $product->id ? 'selected' : '' }} @endif>{{$product->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Supplier</label>
                                <select required name="supplier_id" class="form-control">
                                    <option {{ empty($stock->supplier) === null ? 'selected' : '' }} disabled>Select Supplier</option>
                                    @foreach($suppliers as $supplier)
                                    <option value="{{$supplier->id}}" @if(!empty($stock->supplier)) {{ $stock->supplier->id === $supplier->id ? 'selected' : '' }} @endif>{{$supplier->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Serial Number</label>
                                <input name="serial_number" value="{{$stock->serial_number}}" class="form-control" placeholder="Serial Number">
                            </div>
                            <div class="form-group">
                                <label>Purchased Date</label>
                                <input required name="purchased_date" type="date" value="{{$stock->purchased_date}}" class="form-control" placeholder="Purchased Date">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Quantity</label>
                                <input required id="initial_quantity" name="initial_quantity" value="{{$stock->initial_quantity}}" class="form-control" placeholder="Quantity">
                            </div>
                            <div class="form-group">
                                <label>Unit Price</label>
                                <input required id="unit_price" name="unit_price" value="{{$stock->unit_price}}" class="form-control" placeholder="Unit Price">
                            </div>
                            <div class="form-group">
                                <label>Total Price</label>
                                <input disabled id="total" value="{{$stock->initial_quantity*$stock->unit_price}}" class="form-control" placeholder="Total Price">
                            </div>
                            <div class="form-group">
                                <label>Remarks</label>
                                <input name="remarks" value="{{$stock->remarks}}" class="form-control" placeholder="Remarks">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success form-control">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('#unit_price, #initial_quantity').on('keyup', function(){
        var initial_quantity = $('#initial_quantity').val();
        var unit_price = $('#unit_price').val();
        var total = initial_quantity * unit_price;
        $('#total').val(total);
    });
</script>
@endsection
