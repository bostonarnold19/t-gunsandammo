@extends('layouts.main')
@section('content')
<div class="gallery">
    <div class="text-center">
        <h2>Categories</h2>
    </div>
    <div class="container">
        <div class="row">
            @foreach($categories as $category)
            <div class="col-sm-4">
                <a href="{{route('products', ['category_id' => $category->id])}}">
                    <div class="well">
                        <i class="fa fa-tags fa-4x fa-fw" aria-hidden="true"></i>
                        <div class="text">
                            <h3>{{$category->name}}</h3>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
