@extends('layouts.main')
@section('styles')
<style>
  .brightness {-webkit-filter: brightness(0.30);filter: brightness(0.30);}
</style>
@endsection
@section('content')
<div class="slider">
  <div id="about-slider">
    <div id="carousel-slider" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators visible-xs">
        <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-slider" data-slide-to="1"></li>
        <li data-target="#carousel-slider" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="{{ asset('day/img/slide1.jpg') }}" class="img-responsive brightness" alt="">
          <div class="carousel-caption">
            <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">
              <h2><span>Welcome To Our Site</span></h2>
            </div>
            <div class="col-md-10 col-md-offset-1">
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                <p>Equipped Guns And Ammo</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="{{ asset('day/img/slide2.jpg') }}" class="img-responsive brightness" alt="">
          <div class="carousel-caption">
            <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="1.0s">
              <h2><span>Welcome To Our Site</span></h2>
            </div>
            <div class="col-md-10 col-md-offset-1">
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                <p>Equipped Guns And Ammo</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="{{ asset('day/img/slide3.jpg') }}" class="img-responsive brightness" alt="">
          <div class="carousel-caption">
            <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">
              <h2><span>Welcome To Our Site</span></h2>
            </div>
            <div class="col-md-10 col-md-offset-1">
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                <p>Equipped Guns And Ammo</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
        <i class="fa fa-angle-left"></i>
      </a>
      <a class=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
        <i class="fa fa-angle-right"></i>
      </a>
    </div>
  </div>
</div>
@endsection
