@extends('layouts.main')
@section('content')
<div class="gallery">
    <div class="text-center">
        <h2>Products</h2>
        <p>Click image to reserve</p>
    </div>
    <div class="container">
        @foreach($stocks as $stock)
        <div class="col-md-4">
            <figure class="effect-marley" id="reserve" data-id="{{$stock['product_id']}}">
                <img src="{{ asset('images/'. $stock['product_picture']) }}" alt="" class="img-responsive" />
                <figcaption>
                <h4>{{$stock['product']}}</h4>
                <ul>
                    <li><small>Price: {{number_format($stock['price'], 2)}}</small></li>
                    <li><small>Type: {{$stock['type']}}</small></li>
                    <li><small>Caliber: {{$stock['caliber']}}</small></li>
                    <li><small>Stocks: {{$stock['remaining_stocks']}}</small></li>
                    @if(!empty($cart))
                    @if(!empty($cart->products->find($stock['product_id'])))
                    <li><b class="text-red">Cart</b></li>
                    @endif
                    @endif
                </ul>
                </figcaption>
            </figure>
        </div>
        @endforeach
    </div>
</div>
@endsection
@section('scripts')
<script>
$(document).on('click','[id=reserve]', function(){
    var id = $(this).data('id');
    var route = '{{route('order.product')}}';
    $.ajax({
        method: 'POST',
        url: route,
        data:{
            '_token': $('input[name=_token]').val(),
            'product_id': id
            },
        success: function(data){
            switch(data) {
                case 'added':
                    swal(
                        'Success!',
                        'Product has been added in your Cart',
                        'success'
                        )
                    break;
                case 'deleted':
                    swal(
                        'Success!',
                        'Product has been removed in your Cart',
                        'success'
                    )
                    break;
                default:
                    swal(
                        'Error!',
                        'This user cant order',
                        'error'
                    )
            }
            location.reload();
        }
    });
});
</script>
@endsection
