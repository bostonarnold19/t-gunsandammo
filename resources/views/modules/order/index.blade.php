@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Order <small>Order list</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        @if(!empty($orders->toArray()))
        @foreach($orders as $order)
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Order # {{$order->id}} <span>({{$order->status}})</span></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ID</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Remaining Stocks</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $total = 0;
                            @endphp
                            @foreach($order->products as $product)
                            @php
                                $product_stocks = App\Stock::where('product_id', $product->id)->get();
                                $sold_stocks = [];
                                foreach ($product_stocks as $key => $value) {
                                    $sold_stocks[$key]['quantity'] = $value->transactions->where('status', 'paid')->sum('quantity');
                                }
                                $sold_product = collect($sold_stocks)->sum('quantity');
                                $initial_stocks = $product->stocks->sum('initial_quantity');
                                if(empty($sold_product)) {
                                    $sold_product = 0;
                                }
                                $remaining_stocks = $initial_stocks - $sold_product;
                            @endphp
                            <tr>
                                <td>
                                    <button type="submit" form="product-delete-{{$product->id}}" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                    <form onsubmit="return confirm('Do you want to delete this data?');" id="product-delete-{{$product->id}}" action="{{route('order.detach-product', $product->id)}}" method="POST">
                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                        {{ method_field('PATCH') }}
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                                <td>{{$product->id}}</td>
                                <td>{{$product->name}}</td>
                                <td>
                                    <input min="1" max="{{$remaining_stocks}}" type="number" form="order-form-{{$order->id}}" id="quantity" data-id="{{$product->id}}" name="quantity[{{$product->id}}]" value="{{$product->pivot->quantity}}">
                                </td>
                                <td>{{$remaining_stocks}}</td>
                                <td id="price-{{$product->id}}">{{$product->price}}</td>
                                <td class="sub_total-{{$product->id}}" id="sub_total-{{$product->id}}">{{$product->price * $product->pivot->quantity}}</td>
                            </tr>
                            @php
                                $total = $total + $product->price * $product->pivot->quantity;
                            @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><strong>Total:</strong></td>
                                <td><b class="text-red" id="total">{{$total}}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <form id="order-form-{{$order->id}}" onsubmit="return confirm('Are you sure you want to process this payment?');" action="{{route('order.payment-order',$order->id)}}" method="POST">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success form-control">Process Payment</button>
                </form><br>
                <form onsubmit="return confirm('Are you sure you want to cancel this order?');" action="{{route('order.cancel-order',$order->id)}}" method="POST">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger form-control">Cancel Order</button>
                </form>
            </div>
        </div>
        @endforeach
        @else
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Order list</h3>
                    </div>
                    <div class="panel-body">
                        <center>
                        <i class="fa fa-5x fa-shopping-cart" aria-hidden="true"></i>
                        <p>No order yet</p>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('[id=quantity]').on('keyup', function(){
        var id = $(this).data('id');
        var quantity = $(this).val();
        var price = $('#price-'+id).text();
        var sub_total = quantity * price;
        $('#sub_total-'+id).html(sub_total);
        $('#total').text([].reduce.call($('td[class*="sub_total-"]'), function(sum, ele) {
            return sum + (Number($(ele).text()) || 0);
        }, 0));
    });
</script>
@endsection
