@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Product <small>Product create</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Product create</h3>
            </div>
            <div class="panel-body">
                <form action="{{route('product.store')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-3">
                            <center>
                            <a class="thumbnail" href="#" data-target="#picture">
                                <img id="picture_view" src="{{ asset('images/dummy_product.png') }}" height="200" width="200" class="img-responsive" alt="picture">
                            </a>
                            </center>
                            <div class="form-group">
                                <button type="button" class="btn btn-info form-control" id="file-upload">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                </button>
                                <input type="file" name="picture" id="picture" class="hide" accept="image/*">
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label>Product</label>
                                <input required name="name" value="{{old('name')}}" class="form-control" placeholder="Product">
                            </div>
                            <div class="form-group">
                                <label>Caliber</label>
                                <input name="caliber" value="{{old('caliber')}}" class="form-control" placeholder="Caliber">
                            </div>
                            <div class="form-group">
                                <label>Category</label>
                                <select required name="category_id" class="form-control">
                                    <option {{ old('category_id') === null ? 'selected' : '' }} disabled>Select Category</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{ old('category_id') === $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input required name="price" value="{{old('price')}}" class="form-control" placeholder="Price">
                            </div>
                            <div class="form-group">
                                <label>Discount(%)</label>
                                <input name="discount" value="{{old('discount')}}" class="form-control" placeholder="Discount">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success form-control">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#picture_view').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#picture").change(function(){
        readURL(this);
    });
    $("#file-upload").on('click',function(){
       $("#picture").click();
    });
</script>
@endsection
