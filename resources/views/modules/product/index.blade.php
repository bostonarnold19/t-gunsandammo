@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Product <small>Product list</small></h1>
        <a href="{{route('product.create')}}" class="btn btn-info mgt-b10">Create Product</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Product list</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Caliber</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Discount(%)</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->caliber}}</td>
                                <td>{{empty($product->category) ? '' : $product->category->name}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->discount}}</td>
                                <td>
                                    <a href="{{route('product.edit', $product->id)}}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                                    <button type="submit" form="product-delete-{{$product->id}}" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                    <form onsubmit="return confirm('Do you want to delete this data?');" id="product-delete-{{$product->id}}" action="{{route('product.destroy', $product->id)}}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection
