@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Transaction <small>Transaction list</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Transaction list</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Customer</th>
                                <th>Employee</th>
                                <th>Product</th>
                                <th>Serial Number</th>
                                <th>Quantity</th>
                                <th>Amount Received</th>
                                <th>Discount</th>
                                <th>Product Price</th>
                                <th>Transaction Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transactions as $transaction)
                            <tr>
                                <td>{{$transaction->id}}</td>
                                <td>{{$transaction->customer->first_name}} {{str_limit($transaction->customer->middle_name, 1, '.')}} {{$transaction->customer->last_name}}</td>
                                <td>{{$transaction->employee->first_name}} {{str_limit($transaction->employee->middle_name, 1, '.')}} {{$transaction->employee->last_name}}</td>
                                <td>{{$transaction->stock->product->name}}</td>
                                <td>{{$transaction->stock->serial_number}}</td>
                                <td>{{$transaction->quantity}}</td>
                                <td>{{$transaction->amount_received}}</td>
                                <td>{{$transaction->discount}}</td>
                                <td>{{$transaction->amount_received + $transaction->discount}}</td>
                                <td>{{$transaction->transaction_date}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection
