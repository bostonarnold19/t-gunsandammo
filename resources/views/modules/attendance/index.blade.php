<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>EGA</title>
        <link href="{{ asset('css/hack.css') }}" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">EGA</a>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('partials.helper._message')
                </div>
            </div>
            <div class="row tall-row">
                <div class="col-md-4">
                    <h1>Time In</h1>
                    <hr>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h1>Time Out</h1>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Time In</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($attendances as $attendance)
                            <tr class="active">
                                <td>{{ $attendance->user->id }}</td>
                                <td>{{ $attendance->user->first_name }} {{ $attendance->user->last_name }}</td>
                                <td>{{ date('h:i a', strtotime($attendance->time_in)) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                    <form class="bs-component" action="{{route('attendance.store')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label" for="rfid">RFID</label>
                            <input class="form-control" id="rfid" name="rfid" placeholder="RFID" type="text" autofocus autocomplete="off">
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Time Out</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($attendances as $attendance)
                            @if(!empty($attendance->time_out))
                            <tr class="active">
                                <td>{{ $attendance->user->id }}</td>
                                <td>{{ $attendance->user->first_name }} {{ $attendance->user->last_name }}</td>
                                <td>{{ date('h:i a', strtotime($attendance->time_out)) }}</td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
