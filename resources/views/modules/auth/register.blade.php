@extends('layouts.auth')
@section('content')
<div class="wrap-login100">
    <form class="login100-form validate-form" action="{{route('register')}}" method="POST">
        {{ csrf_field() }}
        <span class="login100-form-logo">
            <i class="zmdi zmdi-landscape"></i>
        </span>
        <span class="login100-form-title p-b-34 p-t-27">
            Register
        </span>
        @include('partials.helper._message')<br>
        <div class="wrap-input100 validate-input" data-validate = "Enter username">
            <input required class="input100" type="text" name="username" placeholder="Username">
            <span class="focus-input100" data-placeholder="&#xf206;"></span>
        </div>
        <div class="wrap-input100 validate-input" data-validate = "Enter first name">
            <input required class="input100" type="text" name="first_name" placeholder="First Name">
            <span class="focus-input100" data-placeholder="&#xf187;"></span>
        </div>
        <div class="wrap-input100 validate-input" data-validate = "Enter middle name">
            <input class="input100" type="text" name="middle_name" placeholder="Middle Name">
            <span class="focus-input100" data-placeholder="&#xf187;"></span>
        </div>
        <div class="wrap-input100 validate-input" data-validate = "Enter last name">
            <input required class="input100" type="text" name="last_name" placeholder="Last Name">
            <span class="focus-input100" data-placeholder="&#xf187;"></span>
        </div>
        <div class="wrap-input100 validate-input" data-validate = "Enter contact number">
            <input class="input100" type="text" name="contact" placeholder="Contact Number">
            <span class="focus-input100" data-placeholder="&#xf2be;"></span>
        </div>
        <div class="wrap-input100 validate-input" data-validate = "Enter email address">
            <input required class="input100" type="email" name="email" placeholder="Email Address">
            <span class="focus-input100" data-placeholder="&#xf15a;"></span>
        </div>
        <div class="container-login100-form-btn">
            <button class="login100-form-btn">
            Register
            </button>
        </div>
        <div class="text-center p-t-90">
            <a class="txt1" href="{{route('login')}}">
                Login
            </a> |
            <a class="txt1" href="#">
                Forgot Password?
            </a>
            <hr>
            <a class="txt1" href="{{route('home')}}">
                Home
            </a><br>
        </div>
    </form>
</div>
@endsection
