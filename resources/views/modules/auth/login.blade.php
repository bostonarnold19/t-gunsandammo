@extends('layouts.auth')
@section('content')
<div class="wrap-login100">
    <form class="login100-form validate-form" action="{{route('login')}}" method="POST">
        {{ csrf_field() }}
        <span class="login100-form-logo">
            <i class="zmdi zmdi-landscape"></i>
        </span>
        <span class="login100-form-title p-b-34 p-t-27">
            Login
        </span>
        @include('partials.helper._message')<br>
        <div class="wrap-input100 validate-input" data-validate = "Enter username">
            <input class="input100" type="text" name="username" placeholder="Username">
            <span class="focus-input100" data-placeholder="&#xf15a;"></span>
        </div>
        <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="password" placeholder="Password">
            <span class="focus-input100" data-placeholder="&#xf191;"></span>
        </div>
        <div class="contact100-form-checkbox">
            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
            <label class="label-checkbox100" for="ckb1">
                Remember me
            </label>
        </div>
        <div class="container-login100-form-btn">
            <button class="login100-form-btn">
            Login
            </button>
        </div>
        <div class="text-center p-t-90">
             <a class="txt1" href="{{route('register')}}">
                Register
            </a> |
            <a class="txt1" href="#">
                Forgot Password?
            </a>
            <hr>
            <a class="txt1" href="{{route('home')}}">
                Home
            </a><br>
        </div>
    </form>
</div>
@endsection
