@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Dashboard <small>Dashboard Home</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="panel panel-default ">
            <div class="panel-body alert-info">
                <div class="col-xs-5">
                    <i class="fa fa-shopping-cart fa-5x"></i>
                </div>
                <div class="col-xs-5 text-right">
                    <b>{{$orders->count()}}</b>
                    <p class="alerts-text">New Orders</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-default ">
            <div class="panel-body alert-info">
                <div class="col-xs-5">
                    <i class="fa fa-exchange fa-5x"></i>
                </div>
                <div class="col-xs-5 text-right">
                    <b>{{ $transactions->count() }}</b>
                    <p class="alerts-text">Sales Today</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-default ">
            <div class="panel-body alert-info">
                <div class="col-xs-5">
                    <i class="fa fa-list-alt fa-5x"></i>
                </div>
                <div class="col-xs-5 text-right">
                    <b>{{ $stocks }}</b>
                    <p class="alerts-text">Stocks</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default ">
            <div class="panel-body alert-info">
                <div class="col-xs-5">
                    <i class="fa fa-money fa-5x"></i>
                </div>
                <div class="col-xs-5 text-right">
                    <b>{{number_format($sales)}}</b>
                    <p class="alerts-text">Total Sales</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default ">
            <div class="panel-body alert-info">
                <div class="col-xs-5">
                    <i class="fa fa-money fa-5x"></i>
                </div>
                <div class="col-xs-5 text-right">
                    <b>{{number_format($capital)}}</b>
                    <p class="alerts-text">Capital</p>
                </div>
            </div>
        </div>
    </div>
</div>
<canvas id="myChart"></canvas>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js"></script>
<script>
    var data_reports = JSON.parse('{!!json_encode($reports)!!}');
    var labels = JSON.parse('{"0":"Jan","2":"Feb","3":"Mar","4":"Apr","5":"May","6":"Jun","7":"Jul","8":"Aug","9":"Sep","10":"Oct","11":"Nov","12":"Dec"}');
    function convertToArray(obj) {
        return Object.keys(obj).map(function(key) {
            return obj[key];
        });
    }
    var labels = convertToArray(labels);
    var data_reports = convertToArray(data_reports);
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: 'Sales',
                data: data_reports,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>
@endsection
