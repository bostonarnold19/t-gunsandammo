@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Cart <small>Product list</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Product list</h3>
            </div>
            <div class="panel-body">
                @if(!empty($cart))
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ID</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $total = 0;
                            @endphp
                            <form onsubmit="return confirm('Are you sure you want to process your order?');" id="process-order" action="{{route('cart.process-order', $cart->id)}}" method="POST">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                            </form>
                            @foreach($cart_products as $product)
                            <tr>
                                <td>
                                    <button type="submit" form="product-delete-{{$product['id']}}" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                    <form onsubmit="return confirm('Do you want to delete this data?');" id="product-delete-{{$product['id']}}" action="{{route('cart.detach-product', $product['id'])}}" method="POST">
                                        {{ method_field('PATCH') }}
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                                <td>{{$product['id']}}</td>
                                <td>{{$product['name']}}</td>
                                <td>
                                    <input min="1" type="number" form="process-order" id="quantity" data-id="{{$product['id']}}" name="quantity[{{$product['id']}}]" value="{{$product['quantity']}}">
                                </td>
                                <td id="price-{{$product['id']}}">{{$product['price']}}</td>
                                <td class="sub_total-{{$product['id']}}" id="sub_total-{{$product['id']}}">{{$product['sub_total']}}</td>
                            </tr>
                            @php
                            $total = $total + $product['sub_total'];
                            @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><strong>Total:</strong></td>
                                <td><b class="text-red" id="total">{{$total}}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <button type="submit" form="process-order" class="btn btn-success form-control">Process Order</button>
                @else
                <center>
                <i class="fa fa-5x fa-shopping-cart" aria-hidden="true"></i>
                <p>There are no items in the cart</p>
                </center>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('[id=quantity]').on('keyup', function(){
        var id = $(this).data('id');
        var quantity = $(this).val();
        var price = $('#price-'+id).text();
        var sub_total = quantity * price;
        $('#sub_total-'+id).html(sub_total);
        $('#total').text([].reduce.call($('td[class*="sub_total-"]'), function(sum, ele) {
            return sum + (Number($(ele).text()) || 0);
        }, 0));
    });
</script>
@endsection
