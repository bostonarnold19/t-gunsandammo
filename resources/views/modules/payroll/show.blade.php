@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('modules.payroll.includes._modalCreate')
        @include('partials.helper._message')
        <h1>{{$user->first_name}} {{$user->last_name}} <small>Payslip list</small></h1>
        <button type="button" class="btn btn-info mgt-b10" data-toggle="modal" data-target="#createPayslip">Generate Payslip</button>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Payslip list</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Grosspay</th>
                                <th>Netpay</th>
                                <th>Inclusive Dates</th>
                                <th>Date Issued</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payslips as $payslip)
                            <tr>
                                <td>{{$payslip->id}}</td>
                                <td>{{$payslip->user->first_name}} {{$payslip->user->last_name}}</td>
                                <td>{{$payslip->grosspay}}</td>
                                <td>{{$payslip->netpay}}</td>
                                <td>{{date("M d, Y", strtotime($payslip->period_start))}} - {{date("M d, Y", strtotime($payslip->period_end))}}</td>
                                <td>{{date("M d, Y", strtotime($payslip->released_date))}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection
