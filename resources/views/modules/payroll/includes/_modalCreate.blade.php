<div class="modal fade" id="createPayslip" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Generate Payslip</h5>
            </div>
            <div class="modal-body">
                <form onsubmit="return confirm('Do you want to save this data?');" action="{{route('payroll.update', $user->id)}}" method="POST">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Year</label>
                                <select required name="year" class="form-control">
                                    <option disabled>Select Year</option>
                                    @for($i = date('Y'); $i >= 2017; $i--)
                                    <option value="{{$i}}" {{ date('Y') === $i ? 'selected' : '' }}>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Month</label>
                                <select required name="month" class="form-control">
                                    <option {{ old('month') === null ? 'selected' : '' }} disabled>Select Month</option>
                                    @foreach(config('date.months') as $month)
                                    <option value="{{$month}}" {{ old('month') === $month ? 'selected' : '' }}>{{$month}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Cut Off</label>
                                <select required name="cut_off" class="form-control">
                                    <option {{ old('cut_off') === null ? 'selected' : '' }} disabled>Select Cut Off</option>
                                    <option value="1st" {{ old('cut_off') === '1st' ? 'selected' : '' }}>1st Cut-Off</option>
                                    <option value="2nd" {{ old('cut_off') === '2nd' ? 'selected' : '' }}>2nd Cut-Off</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success form-control">Generate</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
