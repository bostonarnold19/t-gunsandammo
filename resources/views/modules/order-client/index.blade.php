@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Orders <small>Orders list</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        @if(!empty($orders->toArray()))
        @foreach($orders as $order)
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Order # {{$order->id}} <span>({{$order->status}})</span></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $total = 0;
                            @endphp
                            @foreach($order->products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->pivot->quantity}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->price * $product->pivot->quantity}}</td>
                            </tr>
                            @php
                            $total = $total + $product->price * $product->pivot->quantity;
                            @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><strong>Total:</strong></td>
                                <td><b class="text-red">{{$total}}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <form onsubmit="return confirm('Are you sure you want to cancel this order?');" action="{{route('order-client.cancel-order',$order->id)}}" method="POST">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger form-control">Cancel Order</button>
                </form>
            </div>
        </div>
        @endforeach
        @else
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Orders list</h3>
                    </div>
                    <div class="panel-body">
                        <center>
                        <i class="fa fa-5x fa-shopping-cart" aria-hidden="true"></i>
                        <p>No order yet</p>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
