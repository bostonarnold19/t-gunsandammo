@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('partials.helper._message')
        <h1>Supplier <small>Supplier list</small></h1>
        <a href="{{route('supplier.create')}}" class="btn btn-info mgt-b10">Create Supplier</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-bag"></i> Supplier list</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($suppliers as $supplier)
                            <tr>
                                <td>{{$supplier->id}}</td>
                                <td>{{$supplier->name}}</td>
                                <td>
                                    <a href="{{route('supplier.edit', $supplier->id)}}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                                    <button type="submit" form="supplier-delete-{{$supplier->id}}" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                    <form onsubmit="return confirm('Do you want to delete this data?');" id="supplier-delete-{{$supplier->id}}" action="{{route('supplier.destroy', $supplier->id)}}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection
