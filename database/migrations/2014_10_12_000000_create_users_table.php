<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_id')->nullable();
            $table->string('supplier_id')->nullable();
            $table->string('employee_id')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('initial_quantity')->nullable();
            $table->string('unit_price')->nullable();
            $table->string('remarks')->nullable();
            $table->string('purchased_date')->nullable();
            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_id')->nullable();
            $table->string('name')->nullable();
            $table->string('caliber')->nullable();
            $table->string('price')->nullable();
            $table->string('picture')->default('dummy_product.png');
            $table->string('discount')->nullable();
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id')->nullable();
            $table->string('employee_id')->nullable();
            $table->string('stock_id')->nullable();
            $table->string('quantity')->nullable();
            $table->string('amount_received')->nullable();
            $table->string('discount')->nullable();
            $table->string('transaction_date')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });

        Schema::create('order_transaction', function (Blueprint $table) {
            $table->string('order_id')->nullable();
            $table->string('transaction_id')->nullable();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('username')->nullable();
            $table->longText('address')->nullable();
            $table->string('contact')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('picture')->default('dummy_character.png');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('order_product', function (Blueprint $table) {
            $table->string('order_id')->nullable();
            $table->string('product_id')->nullable();
            $table->string('quantity')->nullable();
        });

        Schema::create('employee_info', function (Blueprint $table) {
            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('rate')->nullable();
            $table->string('rfid')->nullable();
            $table->string('hired_date')->nullable();
            $table->timestamps();
        });

        Schema::create('employee_attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('time_in')->nullable();
            $table->string('time_out')->nullable();
            $table->string('hours_rendered')->nullable();
            $table->timestamps();
        });

        Schema::create('payslips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('hours')->nullable();
            $table->float('pagibig')->nullable();
            $table->float('philhealth')->nullable();
            $table->float('sss')->nullable();
            $table->float('grosspay')->nullable();
            $table->float('netpay')->nullable();
            $table->string('period_start')->nullable();
            $table->string('period_end')->nullable();
            $table->string('released_date')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
        Schema::dropIfExists('products');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('users');
        Schema::dropIfExists('employee_info');
        Schema::dropIfExists('employee_attendances');
        Schema::dropIfExists('payslips');
        Schema::dropIfExists('order_product');
    }
}
