<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'Admin')->first();
        $admin = new User();
        $admin->first_name = 'John';
        $admin->last_name = 'Doe';
        $admin->username = 'admin';
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->roles()->attach($role_admin);
        $admin->employeeInfo()->create(array(
            'employee_id' => $admin->id,
        ));

        $role_cashier = Role::where('name', 'Cashier')->first();
        $cashier = new User();
        $cashier->first_name = 'Jane';
        $cashier->last_name = 'Doe';
        $cashier->username = 'cashier';
        $cashier->email = 'cashier@cashier.com';
        $cashier->password = bcrypt('cashier');
        $cashier->save();
        $cashier->roles()->attach($role_cashier);
        $cashier->employeeInfo()->create(array(
            'employee_id' => $cashier->id,
        ));

        $role_client = Role::where('name', 'Client')->first();
        $client = new User();
        $client->first_name = 'Peter';
        $client->last_name = 'North';
        $client->username = 'client';
        $client->email = 'client@client.com';
        $client->password = bcrypt('client');
        $client->save();
        $client->roles()->attach($role_client);
    }
}
