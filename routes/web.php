<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['middleware' => 'api', 'prefix' => 'api/sale'], function () {

    Route::get('/', [
        'uses' => 'ApiSaleController@index',
        'as' => 'api.sale',
    ]);

    Route::get('/total', [
        'uses' => 'ApiSaleController@total',
        'as' => 'api.sale.total',
    ]);

});

Route::resource('attendance', 'AttendanceController');

Route::get('/', [
    'uses' => 'HomeController@index',
    'as' => 'home',
]);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/products', [
        'uses' => 'HomeController@products',
        'as' => 'products',
    ]);

    Route::get('/categories', [
        'uses' => 'HomeController@categories',
        'as' => 'categories',
    ]);

    Route::post('/order-product', [
        'uses' => 'HomeController@order',
        'as' => 'order.product',
    ]);

    Route::post('/logout', [
        'uses' => 'Auth\LoginController@logout',
        'as' => 'logout',
    ]);

});

Route::group(['middleware' => 'guest'], function () {

    Route::get('/login', [
        'uses' => 'Auth\LoginController@loginForm',
        'as' => 'login',
    ]);

    Route::post('/login', [
        'uses' => 'Auth\LoginController@login',
        'as' => 'login',
    ]);

    Route::get('/register', [
        'uses' => 'Auth\RegisterController@registrationForm',
        'as' => 'register',
    ]);

    Route::post('/register', [
        'uses' => 'Auth\RegisterController@register',
        'as' => 'register',
    ]);

});

Route::group(['middleware' => 'auth', 'prefix' => 'stock', 'middleware' => ['role:Admin']], function () {

    Route::get('/logs', [
        'uses' => 'StockController@logs',
        'as' => 'stock.logs',
    ]);

});

Route::group(['middleware' => 'auth', 'prefix' => 'cart', 'middleware' => ['role:Client']], function () {

    Route::patch('/process-order/{order}', [
        'uses' => 'CartController@processOrder',
        'as' => 'cart.process-order',
    ]);

    Route::patch('/detach-product/{product}', [
        'uses' => 'CartController@detachProduct',
        'as' => 'cart.detach-product',
    ]);

});

Route::group(['middleware' => 'auth', 'prefix' => 'order-client', 'middleware' => ['role:Client']], function () {

    Route::patch('/cancel-order/{order}', [
        'uses' => 'OrderClientController@cancelOrder',
        'as' => 'order-client.cancel-order',
    ]);

});

Route::group(['middleware' => 'auth', 'middleware' => ['role:Client']], function () {

    Route::resource('/order-client', 'OrderClientController');

    Route::resource('/cart', 'CartController');

});

Route::group(['middleware' => 'auth', 'middleware' => ['role:Cashier']], function () {

    Route::resource('/payslip', 'PayslipController');

});

Route::group(['middleware' => 'auth', 'middleware' => ['role:Admin|Cashier']], function () {

    Route::get('/dashboard', [
        'uses' => 'DashboardController@index',
        'as' => 'dashboard',
    ]);

});

Route::group(['middleware' => 'auth', 'middleware' => ['role:Admin']], function () {

    Route::resource('stock', 'StockController');

    Route::resource('product', 'ProductController');

    Route::resource('category', 'CategoryController');

    Route::resource('payroll', 'PayrollController');

    Route::resource('user', 'UserController');

    Route::resource('supplier', 'SupplierController');

});

Route::group(['middleware' => 'auth', 'middleware' => ['role:Admin|Cashier']], function () {

    Route::resource('transaction', 'TransactionController');

    Route::resource('order', 'OrderController');

});

Route::group(['middleware' => 'auth', 'prefix' => 'order', 'middleware' => ['role:Admin|Cashier']], function () {

    Route::patch('/cancel-order/{order}', [
        'uses' => 'OrderController@cancelOrder',
        'as' => 'order.cancel-order',
    ]);

    Route::patch('/payment-order/{order}', [
        'uses' => 'OrderController@paymentOrder',
        'as' => 'order.payment-order',
    ]);

    Route::patch('/detach-product/{product}', [
        'uses' => 'OrderController@detachProduct',
        'as' => 'order.detach-product',
    ]);

});
