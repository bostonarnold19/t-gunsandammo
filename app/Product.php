<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'category_id',
        'name',
        'price',
        'picture',
        'discount',
        'caliber',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function stocks()
    {
        return $this->hasMany('App\Stock');
    }
}
