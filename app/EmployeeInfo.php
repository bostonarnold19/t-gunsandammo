<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeInfo extends Model
{
    protected $table = 'employee_info';

    protected $fillable = [
        'employee_id',
        'rate',
        'rfid',
        'hired_date',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'employee_id');
    }
}
