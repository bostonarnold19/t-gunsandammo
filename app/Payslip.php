<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payslip extends Model
{
    protected $fillable = [
        'user_id',
        'hours',
        'pagibig',
        'philhealth',
        'sss',
        'grosspay',
        'netpay',
        'period_start',
        'period_end',
        'released_date',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
