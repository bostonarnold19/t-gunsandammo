<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'customer_id',
        'product_id',
        'quantity',
        'status',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('quantity');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'customer_id');
    }
}
