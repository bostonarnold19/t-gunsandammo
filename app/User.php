<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;

    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'username',
        'address',
        'picture',
        'email',
        'contact',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function orders()
    {
        return $this->hasMany('App\Order', 'customer_id');
    }

    public function employeeInfo()
    {
        return $this->hasOne('App\EmployeeInfo', 'employee_id');
    }

    public function employeeAttendances()
    {
        return $this->hasMany('App\EmployeeAttendance');
    }

    public function payslips()
    {
        return $this->hasMany('App\Payslip');
    }
}
