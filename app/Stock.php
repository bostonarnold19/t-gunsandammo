<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        'product_id',
        'supplier_id',
        'employee_id',
        'serial_number',
        'initial_quantity',
        'unit_price',
        'remarks',
        'purchased_date',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'employee_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
}
