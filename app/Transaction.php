<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'customer_id',
        'employee_id',
        'stock_id',
        'amount_received',
        'quantity',
        'discount',
        'transaction_date',
        'status',
    ];

    public function employee()
    {
        return $this->belongsTo('App\User', 'employee_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\User', 'customer_id');
    }

    public function stock()
    {
        return $this->belongsTo('App\Stock');
    }
}
