<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAttendance extends Model
{
    protected $table = 'employee_attendances';

    protected $fillable = [
        'user_id',
        'time_in',
        'time_out',
        'hours_rendered',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
