<?php

namespace App\Http\Controllers;

use App\Http\Requests\PayslipRequest;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class PayrollController extends Controller
{
    public function index()
    {
        $users = Role::where('name', 'Cashier')->first()->users;
        return view('modules.payroll.index', compact('users'));
    }

    public function create()
    {
        return;
    }

    public function store(Request $request)
    {
        return;
    }

    public function show($id)
    {
        $user = User::find($id);
        $payslips = $user->payslips;
        return view('modules.payroll.show', compact('payslips', 'user'));
    }

    public function edit($id)
    {
        return;
    }

    public function update(PayslipRequest $request, $id)
    {
        $released_date = Carbon::today();
        $user = User::findOrFail($id);
        if ($request->cut_off === '1st') {
            $ps = Carbon::parse($request->month . $request->year)->startOfMonth();
            $pe = Carbon::parse($request->month . $request->year)->startOfMonth()->addDays(14);
        } elseif ($request->cut_off === '2nd') {
            $ps = Carbon::parse($request->month . $request->year)->startOfMonth()->addDays(15);
            $pe = Carbon::parse($request->month . $request->year)->endOfMonth()->addDays(3);
        } else {
            return redirect()->back()->withErrors('Bad Request!');
        }
        $check_payslip = $user->payslips->where('period_start', $ps)->where('period_end', $pe)->first();
        if (empty($check_payslip)) {
            $attendances = $user->employeeAttendances()
                ->where('created_at', '>=', $ps)
                ->where('created_at', '<=', $pe)
                ->get();
            if (sizeof($attendances)) {
                $rendered_time = $this->computeRenderedTime($attendances);
                $grosspay = $this->computeGrossPay($user->employeeInfo->rate, $rendered_time);
                $netpay = $grosspay;
                $payslip = $this->generatePayslip(
                    $user,
                    $rendered_time,
                    $grosspay,
                    $netpay,
                    $ps,
                    $pe,
                    $released_date
                );
                Session::flash('flash_message', 'Payslip has been generated!');
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors('No attendance!');
            }
        } else {
            return redirect()->back()->withErrors('Payslip exist!');
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        return;
    }

    private function generatePayslip($user, $rendered_time, $grosspay, $netpay, $ps, $pe, $rd)
    {
        $data = [
            'hours' => $rendered_time,
            'grosspay' => $grosspay,
            'netpay' => $netpay,
            'period_start' => $ps,
            'period_end' => $pe,
            'released_date' => $rd,
        ];
        return $user->payslips()->create($data);
    }

    private function computeGrossPay($rate, $rendered_time)
    {
        return ($rate * $rendered_time);
    }

    private function computeRenderedTime($attendances)
    {
        $total_hours = 0;
        $total_minutes = 0;
        foreach ($attendances as $attendance) {
            if (empty($attendance->hours_rendered)) {
                $attendance->hours_rendered = "0:0";
            }
            $x = explode(':', $attendance->hours_rendered);
            $total_hours = $total_hours + $x[0];
            $total_minutes = $total_minutes + $x[1];
        }
        $computed_time = intdiv($total_minutes, 60) . ':' . ($total_minutes % 60);
        $exploded_time = explode(':', $computed_time);
        $total_hours = $total_hours + (int) $exploded_time[0];
        $rendered_total = sprintf("%d:%d", $total_hours, $exploded_time[1]);
        $rendered_total = (int) $rendered_total;
        return $rendered_total;
    }
}
