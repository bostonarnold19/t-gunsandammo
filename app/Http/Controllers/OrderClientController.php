<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class OrderClientController extends Controller
{
    public function index()
    {
        $orders = auth()->user()->orders()->where('status', 'Processed')->get()->sortByDesc('id');
        return view('modules.order-client.index', compact('orders'));
    }

    public function create()
    {
        return;
    }

    public function store(Request $request)
    {
        return;
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        return;
    }

    public function update(Request $request, $id)
    {
        return;
    }

    public function destroy($id)
    {
        return;
    }

    public function cancelOrder(Request $request, $id)
    {
        $cart = auth()->user()->orders()->find($id);
        $data = [
            'status' => 'Cancelled',
        ];
        $cart->update($data);
        Session::flash('flash_message', 'Order has been cancelled!');
        return redirect()->route('order-client.index');
    }
}
