<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all()->except(1);
        return view('modules.user.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('modules.user.create', compact('roles'));
    }

    public function store(UserRequest $request)
    {
        $user = new User;
        $code = 'ega-' . mt_rand(5, 99999);
        $request->request->add(['username' => $code]);
        $request->request->add(['password' => bcrypt($code)]);
        $user->fill($request->all())->save();
        $user->roles()->attach($request->role_id);
        $data = array(
            'employee_id' => $user->id,
            'rate' => $request->rate,
            'rfid' => $request->rfid,
            'hired_date' => $request->hired_date,
        );
        $user->employeeInfo()->create($data);
        return redirect()->route('user.index');
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        return view('modules.user.edit', compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->all());
        $user->roles()->detach();
        $user->roles()->attach($request->role_id);
        $data = array(
            'employee_id' => $user->id,
            'rate' => $request->rate,
            'rfid' => $request->rfid,
            'hired_date' => $request->hired_date,
        );
        $user->employeeInfo()->update($data);
        return redirect()->route('user.index');
    }

    public function destroy($id)
    {
        return;
    }
}
