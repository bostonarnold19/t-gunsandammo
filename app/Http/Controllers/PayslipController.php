<?php

namespace App\Http\Controllers;

class PayslipController extends Controller
{
    public function index()
    {
        $payslips = auth()->user()->payslips;
        return view('modules.payslip.index', compact('payslips'));
    }
}
