<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Stock;
use App\Transaction;
use Illuminate\Http\Request;
use Session;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::where('status', 'Processed')->get();
        return view('modules.order.index', compact('orders'));
    }

    public function create()
    {
        return;
    }

    public function store(Request $request)
    {
        return;
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        return;
    }

    public function update(Request $request, $id)
    {
        return;

    }

    public function destroy($id)
    {
        return;
    }

    public function cancelOrder(Request $request, $id)
    {
        $order = Order::find($id);
        $data = [
            'status' => 'Cancelled',
        ];
        $order->update($data);
        Session::flash('flash_message', 'Order has been cancelled!');
        return redirect()->route('order.index');
    }

    public function paymentOrder(Request $request, $id)
    {
        $this->updateOrderQuantity($request->quantity, $id);
        $order = Order::find($id);
        foreach ($order->products as $order_product) {
            $product_remaining_stocks = $this->checkRemainingStocks($order_product->id);
            if ($product_remaining_stocks >= $order_product->pivot->quantity) {
                $product_stocks = Stock::where('product_id', $order_product->id)->get();
                $stocks = [];
                foreach ($product_stocks as $key => $value) {
                    $stocks[$key]['id'] = $value->id;
                    $stocks[$key]['remaining'] = $value->initial_quantity - $value->transactions->where('status', 'paid')->sum('quantity');
                    $stocks[$key]['purchased_date'] = $value->purchased_date;
                    if ($stocks[$key]['remaining'] === 0) {
                        unset($stocks[$key]);
                    }
                }
                $stocks = collect($stocks)->sortBy('purchased_date');
                $product_quantity = $order_product->pivot->quantity;
                foreach ($stocks as $stock) {
                    if ($stock['remaining'] >= $product_quantity) {
                        $this->processPayment(
                            $order->user->id,
                            $stock['id'],
                            $product_quantity,
                            $order_product
                        );
                        break;
                    } else {
                        $this->processPayment(
                            $order->user->id,
                            $stock['id'],
                            $stock['remaining'],
                            $order_product
                        );
                        $product_quantity = $product_quantity - $stock['remaining'];
                    }
                }
            } else {
                $order_product->pivot->delete();
                if (empty($order_product->get()->toArray())) {
                    $order_id = $order->id;
                    $order->delete();
                    return redirect()->back()->withErrors('This order cant process. No stocks available. Order #' . $order_id . ' has been removed!');
                }
            }
        }
        $data = [
            'status' => 'Completed',
        ];
        $order->update($data);
        Session::flash('flash_message', 'The order has been completed. Order #' . $order->id . '!');
        return redirect()->back();
    }

    private function processPayment($customer_id, $stock_id, $product_quantity, $product)
    {
        $perc = '.' . $product->discount;
        $amount_received = ((float) $product->price * $product_quantity) - ((float) $product->price * (float) $perc);
        $discount = ((float) $product->price * (float) $perc);
        $data = array(
            'customer_id' => $customer_id,
            'employee_id' => auth()->user()->id,
            'stock_id' => $stock_id,
            'quantity' => $product_quantity,
            'amount_received' => $amount_received,
            'discount' => $discount,
            'transaction_date' => date('Y-m-d'),
            'status' => 'paid',
        );
        $transaction = new Transaction;
        $transaction->fill($data)->save();
        return;
    }

    private function checkRemainingStocks($product_id)
    {
        $total_product_stocks = Product::find($product_id)->stocks->sum('initial_quantity');
        $product_stocks = Stock::where('product_id', $product_id)->get();
        $sold_stocks = [];
        foreach ($product_stocks as $key => $value) {
            $sold_stocks[$key]['quantity'] = $value->transactions->where('status', 'paid')->sum('quantity');
        }
        $sold_stock = collect($sold_stocks)->sum('quantity');
        $remaining_stocks = $total_product_stocks - $sold_stock;
        return $remaining_stocks;
    }

    private function updateOrderQuantity($quantity, $order_id)
    {
        $order = Order::find($order_id);
        foreach ($quantity as $key => $value) {
            $product = $order->products()->where('id', $key)->first();
            $product->pivot->quantity = $value;
            $product->pivot->save();
        }
        return;
    }

    public function detachProduct(Request $request, $id)
    {
        $order = Order::find($request->order_id);
        $order->products()->detach($id);
        if (empty($order->products()->get()->toArray())) {
            $order->delete();
        }
        Session::flash('flash_message', 'Product has been removed from Order #' . $order->id . '!');
        return redirect()->back();
    }
}
