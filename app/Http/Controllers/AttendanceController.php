<?php

namespace App\Http\Controllers;

use App\EmployeeAttendance;
use App\EmployeeInfo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class AttendanceController extends Controller
{
    public function index()
    {
        $attendances = EmployeeAttendance::where('created_at', '>=', date('Y-m-d') . ' 00:00:00')->get();
        return view('modules.attendance.index', compact('attendances'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $employee = EmployeeInfo::where('rfid', $request->rfid)->first();
        if (!empty($employee)) {
            $attendance = $employee->user->employeeAttendances->where('created_at', '>=', date('Y-m-d') . ' 00:00:00')->first();
            $date_now = date('Y-m-d H:i:s');
            if (empty($attendance)) {
                $data = array(
                    'time_in' => $date_now,
                );
                $employee->user->employeeAttendances()->create($data);
                Session::flash('flash_message', 'Time in: ' . date('h:i a'));
            } else {
                if (empty($attendance->time_out)) {
                    $in = Carbon::parse($attendance->time_in);
                    $out = Carbon::parse($date_now);
                    $rendered_time = $in->diff($out);
                    $hours_rendered = $rendered_time->format('%h:%i');
                    $data = array(
                        'time_out' => $date_now,
                        'hours_rendered' => $hours_rendered,
                    );
                    $employee->user->employeeAttendances()->get()->last()->update($data);
                    Session::flash('flash_message', 'Time out: ' . date('h:i a'));
                } else {
                    return redirect()->back()->withErrors('Attendance has been submitted for this day!');
                }
            }
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors('User not found!');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
