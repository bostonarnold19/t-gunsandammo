<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupplierRequest;
use App\Supplier;
use Session;

class SupplierController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::all();
        return view('modules.supplier.index', compact('suppliers'));
    }

    public function create()
    {
        return view('modules.supplier.create');
    }

    public function store(SupplierRequest $request)
    {
        $supplier = new Supplier;
        $supplier->fill($request->all())->save();
        Session::flash('flash_message', 'Supplier has been added!');
        return redirect()->route('supplier.index');
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        $supplier = Supplier::find($id);
        return view('modules.supplier.edit', compact('supplier'));
    }

    public function update(SupplierRequest $request, $id)
    {
        $supplier = Supplier::find($id);
        $supplier->update($request->all());
        Session::flash('flash_message', 'Supplier has been updated!');
        return redirect()->route('supplier.index');
    }

    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        $supplier->delete();
        Session::flash('flash_message', 'Supplier has been deleted!');
        return redirect()->back();
    }
}
