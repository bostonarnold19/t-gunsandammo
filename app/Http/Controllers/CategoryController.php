<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Session;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('modules.category.index', compact('categories'));
    }

    public function create()
    {
        return view('modules.category.create');
    }

    public function store(CategoryRequest $request)
    {
        $category = new Category;
        $category->fill($request->all())->save();
        Session::flash('flash_message', 'Category has been added!');
        return redirect()->route('category.index');
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('modules.category.edit', compact('category'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = Category::find($id);
        $category->update($request->all());
        Session::flash('flash_message', 'Category has been updated!');
        return redirect()->route('category.index');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        Session::flash('flash_message', 'Category has been deleted!');
        return redirect()->back();
    }
}
