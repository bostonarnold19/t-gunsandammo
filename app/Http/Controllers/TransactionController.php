<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        if (empty($request->customer_id)) {
            $transactions = Transaction::all();
        } else {
            $transactions = Transaction::where('customer_id', $request->customer_id)->get();
        }
        return view('modules.transaction.index', compact('transactions'));
    }

    public function create()
    {
        return;
    }

    public function store(Request $request)
    {
        return;
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        return;
    }

    public function update(Request $request, $id)
    {
        return;
    }

    public function destroy($id)
    {
        return;
    }
}
