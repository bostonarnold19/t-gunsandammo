<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationRequest;
use App\Mail\RegistrationMail;
use App\Role;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Session;

class RegisterController extends Controller
{
    public function registrationForm()
    {
        return view('modules.auth.register');
    }

    public function register(RegistrationRequest $request)
    {
        $code = 'EGA-' . mt_rand(5, 99999);
        $role_client = Role::where('name', 'Client')->first();
        $user = new User;
        $request->request->add(['password' => bcrypt($code)]);
        $user->fill($request->all())->save();
        $user->roles()->attach($role_client);
        Mail::to($request->email)->send(new RegistrationMail($code));
        Session::flash('flash_message', 'Please check your email to get your password!');
        return redirect()->route('login');
    }
}
