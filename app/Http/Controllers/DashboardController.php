<?php

namespace App\Http\Controllers;

use App\Order;
use App\Stock;
use App\Transaction;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $orders = Order::where('status', 'Processed')->get();
        $transactions = Transaction::where('transaction_date', date('Y-m-d'))->get();
        $capital = Stock::all()->sum(function ($value) {
            return $value->initial_quantity * $value->unit_price;
        });
        $sales = Transaction::all()->sum('amount_received');
        $stocks = Stock::all()->sum('initial_quantity') - Transaction::all()->sum('quantity');
        $transaction_per_month = DB::table('transactions')
            ->where('created_at', '>=', Carbon::now()->startOfYear())
            ->where('created_at', '>=', Carbon::now()->subYear())
            ->get(['transaction_date', 'amount_received'])
            ->groupBy(function ($date) {
                return Carbon::parse($date->transaction_date)->format('M');
            });
        $reports = [
            "Jan" => 0,
            "Feb" => 0,
            "Mar" => 0,
            "Apr" => 0,
            "May" => 0,
            "Jun" => 0,
            "Jul" => 0,
            "Aug" => 0,
            "Sep" => 0,
            "Oct" => 0,
            "Nov" => 0,
            "Dec" => 0,
        ];
        foreach ($transaction_per_month as $key => $value) {
            $reports[$key] = $value->sum('amount_received');
        }
        return view('modules.dashboard.index', compact(
            'orders',
            'sales',
            'capital',
            'transactions',
            'stocks',
            'reports'
        ));
    }
}
