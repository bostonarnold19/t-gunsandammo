<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class CartController extends Controller
{
    public function index()
    {
        $cart = auth()->user()->orders()->where('status', 'Cart')->first();
        if (!empty($cart)) {
            $products = $cart->products()->get();
            $cart_products = [];
            foreach ($products as $key => $value) {
                $cart_products[$key]['id'] = $value->id;
                $cart_products[$key]['name'] = $value->name;
                $cart_products[$key]['quantity'] = $value->pivot->quantity;
                $cart_products[$key]['price'] = $value->price;
                $cart_products[$key]['sub_total'] = $value->price * $value->pivot->quantity;
            }
        }
        return view('modules.cart.index', compact('cart_products', 'cart'));
    }

    public function processOrder(Request $request, $id)
    {
        $cart = auth()->user()->orders()->find($id);
        $data = [
            'status' => 'Processed',
        ];
        $cart->update($data);
        foreach ($request->quantity as $key => $value) {
            $product = $cart->products()->where('id', $key)->first();
            $product->pivot->quantity = $value;
            $product->pivot->save();
        }
        return redirect()->route('order-client.index');
    }

    public function detachProduct(Request $request, $id)
    {
        $cart = auth()->user()->orders()->where('status', 'Cart')->first();
        $cart->products()->detach($id);
        if (empty($cart->products()->get()->toArray())) {
            $cart->delete();
        }
        Session::flash('flash_message', 'Product has been removed from cart!');
        return redirect()->back();
    }
}
