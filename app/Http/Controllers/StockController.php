<?php

namespace App\Http\Controllers;

use App\Http\Requests\StockRequest;
use App\Product;
use App\Stock;
use App\Supplier;
use Session;

class StockController extends Controller
{
    public function index()
    {
        $collection_products = Product::all();
        $stocks = [];
        foreach ($collection_products as $product) {
            $product_stocks = Stock::where('product_id', $product->id)->get();
            $sold_stocks = [];
            foreach ($product_stocks as $key => $value) {
                $sold_stocks[$key]['quantity'] = $value->transactions->where('status', 'paid')->sum('quantity');
            }
            $sold_stock = collect($sold_stocks)->sum('quantity');
            $stocks[] = array(
                'id' => $product->id,
                'product' => $product->name,
                'type' => $product->category->name,
                'caliber' => $product->caliber,
                'remaining_stocks' => $product->stocks->sum('initial_quantity') - $sold_stock,
                'total_stocks' => $product->stocks->sum('initial_quantity'),
                'total_sold' => $sold_stock,
                'price' => $product->price,
            );
        }
        return view('modules.stock.index', compact('stocks'));
    }

    public function create()
    {
        $products = Product::all();
        $suppliers = Supplier::all();
        return view('modules.stock.create', compact('products', 'suppliers'));
    }

    public function store(StockRequest $request)
    {
        $data = $request->all();
        $data['employee_id'] = auth()->user()->id;
        $stock = new Stock;
        $stock->fill($data)->save();
        Session::flash('flash_message', 'Stock has been added!');
        return redirect()->route('stock.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $stock = Stock::find($id);
        $products = Product::all();
        $suppliers = Supplier::all();
        return view('modules.stock.edit', compact('stock', 'products', 'suppliers'));
    }

    public function update(StockRequest $request, $id)
    {
        $stock = Stock::find($id);
        $stock->update($request->all());
        Session::flash('flash_message', 'Stock has been updated!');
        return redirect()->route('stock.logs');
    }

    public function destroy($id)
    {
        $stock = Stock::find($id);
        $stock->delete();
        Session::flash('flash_message', 'Stock has been deleted!');
        return redirect()->back();
    }

    public function logs()
    {
        $stocks = Stock::all();
        return view('modules.stock.logs', compact('stocks'));
    }
}
