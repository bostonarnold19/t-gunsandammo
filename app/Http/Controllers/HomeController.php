<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\Stock;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('modules.home.index');
    }

    public function categories()
    {
        $categories = Category::all();
        return view('modules.home.categories', compact('categories'));
    }

    public function products(Request $request)
    {
        $category = Category::findOrFail($request->category_id);
        $collection_products = $category->products;
        $stocks = [];
        foreach ($collection_products as $product) {
            $product_stocks = Stock::where('product_id', $product->id)->get();
            $sold_stocks = [];
            foreach ($product_stocks as $key => $value) {
                $sold_stocks[$key]['quantity'] = $value->transactions->where('status', 'paid')->sum('quantity');
            }
            $sold_stock = collect($sold_stocks)->sum('quantity');
            $stocks[] = array(
                'product_id' => $product->id,
                'product_picture' => $product->picture,
                'product' => $product->name,
                'type' => $product->category->name,
                'caliber' => $product->caliber,
                'remaining_stocks' => $product->stocks->sum('initial_quantity') - $sold_stock,
                'price' => $product->price,
            );
        }
        $cart = auth()->user()->orders()->where('status', 'Cart')->first();
        return view('modules.home.products', compact('stocks', 'cart'));
    }

    public function order(Request $request)
    {
        if (auth()->user()->hasRole('Client')) {
            $cart = auth()->user()->orders()->where('status', 'Cart')->first();
            if (!empty($cart)) {
                $product_cart = $cart->products()->find($request->product_id);
                if (!empty($product_cart)) {
                    $cart->products()->detach($request->product_id);
                    if (empty($cart->products()->get()->toArray())) {
                        $cart->delete();
                        return response()->json('deleted');
                    }
                } else {
                    $cart->products()->attach($request->product_id, array('quantity' => 1));
                    return response()->json('added');
                }
            } else {
                $new_cart = new Order;
                $data = array(
                    'customer_id' => auth()->user()->id,
                    'status' => 'Cart',
                );
                $new_cart->fill($data)->save();
                $new_cart->products()->attach($request->product_id, array('quantity' => 1));
                return response()->json('added');
            }
        } else {
            return response()->json('error');
        }
    }
}
