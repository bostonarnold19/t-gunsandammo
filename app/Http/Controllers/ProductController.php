<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\ProductRequest;
use App\Product;
use Image;
use Session;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('modules.product.index', compact('products'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('modules.product.create', compact('categories'));
    }

    public function store(ProductRequest $request)
    {
        $data = $request->all();
        if ($request->has('picture')) {
            $picture = $request->file('picture');
            $filename = time() . '.' . $picture->getClientOriginalExtension();
            $data['picture'] = $filename;
            $background = Image::canvas(480, 360);
            $image = Image::make($picture)->resize(480, 360, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });
            $background->insert($image, 'center');
            $background->save(public_path('images/' . $filename));
        }
        $product = new Product;
        $product->fill($data)->save();
        Session::flash('flash_message', 'Product has been added!');
        return redirect()->route('product.index');
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        return view('modules.product.edit', compact('product', 'categories'));
    }

    public function update(ProductRequest $request, $id)
    {
        $data = $request->all();
        if ($request->has('picture')) {
            $picture = $request->file('picture');
            $filename = time() . '.' . $picture->getClientOriginalExtension();
            $data['picture'] = $filename;
            $background = Image::canvas(480, 360);
            $image = Image::make($picture)->resize(480, 360, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });
            $background->insert($image, 'center');
            $background->save(public_path('images/' . $filename));
        }
        $product = Product::find($id);
        $product->update($data);
        Session::flash('flash_message', 'Product has been updated!');
        return redirect()->route('product.index');
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        Session::flash('flash_message', 'Product has been deleted!');
        return redirect()->back();
    }
}
