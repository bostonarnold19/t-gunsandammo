<?php

namespace App\Http\Controllers;

use App\Transaction;
use GuzzleHttp\Client;

class ApiSaleController extends Controller
{
    public function index()
    {
        $sales = Transaction::all()->sum('amount_received');
        return response()->json($sales);
    }

    public function total()
    {
        $client1 = new Client;
        $res1 = $client1->get('https://ega-pampanga.herokuapp.com/api/sale');

        $client2 = new Client;
        $res2 = $client2->get('https://ega-manila.herokuapp.com/api/sale');

        $data = [
            'pampanga' => $res1->getBody(),
            'manila' => $res2->getBody(),
        ];
        return response()->json($data);
    }
}
