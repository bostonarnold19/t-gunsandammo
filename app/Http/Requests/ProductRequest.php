<?php

namespace App\Http\Requests;

use App\Product;
use Illuminate\Foundation\Http\FormRequest;
use Route;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'name' => 'required|max:191|unique:products',
                        'caliber' => 'max:191',
                        'price' => 'required|numeric',
                        'category_id' => 'required|max:191|exists:categories,id',
                        'picture' => 'sometimes|required|image|mimes:jpeg,jpg,png|max:2048',
                        'discount' => 'numeric|max:100',
                    ];
                }
            case 'PATCH':
                {
                    $product_id = Route::current()->parameters['product'];
                    $product = Product::findOrFail($product_id);
                    return [
                        'name' => 'required|max:191|unique:products,name,' . $product->id,
                        'caliber' => 'max:191',
                        'price' => 'required|numeric',
                        'category_id' => 'required|exists:categories,id',
                        'picture' => 'sometimes|required|image|mimes:jpeg,jpg,png|max:2048',
                        'discount' => 'numeric|max:100',
                    ];
                }
            default:break;
        }
    }
}
