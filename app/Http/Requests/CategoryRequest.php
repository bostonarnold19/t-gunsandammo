<?php

namespace App\Http\Requests;

use App\Category;
use Illuminate\Foundation\Http\FormRequest;
use Route;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'name' => 'required|max:191|unique:categories',
                    ];
                }
            case 'PATCH':
                {
                    $category_id = Route::current()->parameters['category'];
                    $category = Category::findOrFail($category_id);
                    return [
                        'name' => 'required|max:191|unique:categories,name,' . $category->id,
                    ];
                }
            default:break;
        }
    }
}
