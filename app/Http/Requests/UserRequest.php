<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Route;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'first_name' => 'required|max:191',
                        'last_name' => 'required|max:191',
                        'middle_name' => 'max:191',
                        'rate' => 'sometimes|required|numeric',
                        'rfid' => 'sometimes|required|unique:employee_info,rfid',
                        'hired_date' => 'sometimes|required',
                        'role_id' => 'required|exists:roles,id',
                    ];
                }
            case 'PATCH':
                {
                    $user_id = Route::current()->parameters['user'];
                    $user = User::findOrFail($user_id);
                    return [
                        'first_name' => 'required|max:191',
                        'last_name' => 'required|max:191',
                        'middle_name' => 'max:191',
                        'rate' => 'sometimes|required|numeric',
                        'rfid' => 'sometimes|required|unique:employee_info,rfid,' . $user->employee_info->rfid,
                        'hired_date' => 'sometimes|required',
                        'role_id' => 'required|exists:roles,id',
                    ];
                }
            default:break;
        }
    }
}
